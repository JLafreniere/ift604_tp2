const AUTO_REFRESH_INTERVAL = 120000;
var app = angular.module('myapp', []);
var sx;

function getMatchesData($scope, $http){
    $http({
      method: 'GET',
      url: 'http://localhost:23567/matches'
    }).then(function successCallback(response) {
        $scope.data = updateScore(response.data);
      }, function errorCallback(response) {
      });
}


function postBet($scope, $http, matchId, teamId, user, amount){
    $http({
      method: 'POST',
      url: 'http://localhost:23567/matches/'+matchId+"/equipes/"+teamId+"/bet",
      data: {
        "user": user,
        "amount": amount
      }
    }).then(function successCallback(response) {
       $scope.getBets()
       $scope.refresh();
      }, function errorCallback(response) {
      });
}

function getBets($scope, $http, user){
  $http({
    method: 'GET',
    url: "http://localhost:23567/bets/"+user
  }).then(function successCallback(response) {
      $scope.bets = response.data;

      let acc = 0;

      for(let i = 0; i < $scope.bets.length; i++){
        acc += $scope.bets[i].valeurFinale;
      }

      $scope.betsValue = acc;
    }, function errorCallback(response) {

    });
}

function updateScore(data)
{
  let arr = [];
    for(let i in data){
	    arr.push(data[i]);
    }

    for(let i = 0; i < arr.length; i++){
        arr[i].equipe1.pointage = arr[i].equipe1.buts.length;
        arr[i].equipe2.pointage = arr[i].equipe2.buts.length;
    }
    return arr;
}

app.controller('MainCtrl', ['$scope', '$window', '$http' , function($scope, $window, $http) {
  $scope.betsValue = 0;
  $scope.bets = [];
  $scope.user = getCookie("betUserInput");

  $scope.getBets = function(user){
    getBets($scope, $http, $scope.user);
  }



  $scope.getMatchName = function (id){
    let match = $scope.getMatch(id);
    return match.equipe1.nom + " VS. " + match.equipe2.nom;
  }

  $scope.bet = function(matchId, teamId, user, amount){
    if(amount <= 0){
      alert("Montant invalide!")
    } else {
      postBet($scope, $http, matchId, teamId, user, amount);
    }
  }

  $scope.getMatch = function (matchId){
    for(let i = 0; i < $scope.data.length; i++){
      if($scope.data[i].id == matchId){
        return $scope.data[i];
      }
    }
    return null;
  }

  $scope.getTeamName = (matchId, teamId)=>{
    let match = $scope.getMatch(matchId);
    if(teamId == 1){
      try{
      return match.equipe1.nom;
      } catch (e) {
        console.log(matchId);
      }
    } else return match.equipe2.nom;
  }

  $scope.refresh = function() { 
    $window.getMatchesData($scope, $http);
  } 
  
  $scope.areBetsAllowed = function(date){
    let i =  (new Date() - moment(date, "DD/MM/YYYY hh:mm:ss").toDate())/1000/60;
    let periode = Math.floor(i/20) + 1;
    return periode <= 2;
  }

  $scope.hasBets = function(){
    return $scope.bets.length > 0;
  }


  $scope.formatDate = function(x){
    try{

      let i =  (new Date() - moment(x, "DD/MM/YYYY hh:mm:ss").toDate())/1000/60;
      let periode = Math.floor(i/20) + 1;
      let secondes = Math.floor((i % 1) * 60);
      let minutes = Math.floor(i % 20);
      let padding = (secondes<10)?"0":""
      if(periode <= 3){
        return "Période "+ periode+":     "+ minutes + ":"+padding+secondes; 
      }
      return "Match terminé";
    } catch (e) {
      console.log(e);
      return e;
    }
    
  };
  $scope.relativeDate = function(x, y, penalite = false){
    try{

      let i =  (moment(x, "DD/MM/YYYY hh:mm:ss").toDate() - moment(y, "DD/MM/YYYY hh:mm:ss").toDate())/1000/60;
      let periode = Math.floor(i/20) + 1;
      let secondes = Math.floor((i % 1) * 60);
      let minutes = Math.floor(i % 20);
      let ongoing =  ((new Date() - moment(x, "DD/MM/YYYY hh:mm:ss").toDate())/1000/60);
      let isOngoing = false;
      if(ongoing <= 2 && ongoing >= 0){
        isOngoing = true;
      } 
      
    
      let padding = (secondes<10)?"0":""
    
      let val = "Période "+ periode+":     "+ minutes + ":"+padding+secondes; 
      if(penalite && isOngoing){
        val += " (En cours)";
      }
      return val;
      
    } catch (e) {
      console.log(e);
      return "WAAA";
    }
  }
  $window.getMatchesData($scope, $http);
  $scope.getBets($scope.user);
  sx = $scope;
}]);

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});
  });

$(document).ready(function () {
  $("#betUserInput").val(getCookie("betUserInput"));

  $("#betUserInput").on('input',function(e){
    document.cookie = "betUserInput="+$(this).val();
  });
});

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function refreshData () {
  $(".progress").removeClass("hide");
  sx.getBets();
  sx.$apply();
  setTimeout(function(){ $(".progress").addClass("hide"); }, 1000);
}

var interval = setInterval(function () { refreshData(); }, AUTO_REFRESH_INTERVAL);

function distance(lon1, lat1, lon2, lat2) {
  var R = 6371; // Radius of the earth in km
  var dLat = (lat2-lat1).toRad();  // Javascript functions in radians
  var dLon = (lon2-lon1).toRad(); 
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
          Math.sin(dLon/2) * Math.sin(dLon/2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

function verifPosition(pos) {
  var dist = distance(pos.coords.longitude, pos.coords.latitude, -71.899581, 45.398484);
  if (dist > 10)
  {
    $('#modalSherbrooke').modal('open');
  }
}



$(document).ready(function(){
  $('.modal').modal();
  navigator.geolocation.getCurrentPosition(verifPosition, null , { timeout: 0 });
});