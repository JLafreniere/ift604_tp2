function sampleMatchData() {
    let obj = {
        "1": {
            "id": 1,
            "date": "27/11/2019 13:18:39",
            "equipe1": {
                "nom": "Blues",
                "penalites": [{
                    "joueur": 21,
                    "temps": "27/11/2019 13:18:39"
                }],
                "buts": [{
                    "player": 16,
                    "date": "27/11/2019 13:19:39"
                }, {
                    "player": 9,
                    "date": "27/11/2019 13:21:39"
                }]
            },
            "equipe2": {
                "nom": "Reds",
                "penalites": [],
                "buts": [{
                    "player": 12,
                    "date": "27/11/2019 13:18:53"
                }]
            }
        },
        "2": {
            "id": 2,
            "date": "25/11/2019 19:25:39",
            "equipe1": {
                "nom": "Oranges",
                "penalites": [],
                "buts": []
            },
            "equipe2": {
                "nom": "Greens",
                "penalites": [],
                "buts": [{
                    "player": 6,
                    "date": "25/11/2019 20:08:59"
                }]
            }
        },
        "3": {
            "id": 3,
            "date": "25/11/2019 20:07:39",
            "equipe1": {
                "nom": "Yellows",
                "penalites": [{
                    "joueur": 24,
                    "temps": "25/11/2019 20:08:19"
                }],
                "buts": [{
                    "player": 14,
                    "date": "25/11/2019 20:07:59"
                }]
            },
            "equipe2": {
                "nom": "Browns",
                "penalites": [],
                "buts": [{
                    "player": 27,
                    "date": "25/11/2019 20:07:39"
                }]
            }
        },
        "4": {
            "id": 4,
            "date": "25/11/2019 20:07:39",
            "equipe1": {
                "nom": "Cyans",
                "penalites": [{
                    "joueur": 10,
                    "temps": "25/11/2019 20:10:19"
                }],
                "buts": []
            },
            "equipe2": {
                "nom": "Bluebirds",
                "penalites": [],
                "buts": [{
                    "player": 9,
                    "date": "25/11/2019 20:09:59"
                }]
            }
        },
        "5": {
            "id": 5,
            "date": "25/11/2019 18:07:39",
            "equipe1": {
                "nom": "TemperateYellow",
                "penalites": [],
                "buts": [{
                    "player": 37,
                    "date": "25/11/2019 18:08:39"
                }, {
                    "player": 27,
                    "date": "25/11/2019 18:09:39"
                }]
            },
            "equipe2": {
                "nom": "CyanOrange",
                "penalites": [],
                "buts": []
            }
        }
    }
    return obj;
};

/*
let reducedData = [];
let data = getMatches();
for (let i = 0; i < data.length - 1; i++) {
    reducedData.push(data[i]);
}

*/
